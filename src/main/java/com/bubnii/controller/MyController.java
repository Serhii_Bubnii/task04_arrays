package com.bubnii.controller;

import com.bubnii.model.array.OperationsOnArrays;
import com.bubnii.model.queue.DroidFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

import static com.bubnii.model.log.ClassName.getCurrentClassName;

public class MyController {

    private Scanner scanner = new Scanner(System.in);
    private static int[] arrFirst;
    private static int[] arrSecond;
    private static final Logger logger = LogManager.getLogger(getCurrentClassName());

    public static void myFactory(int operation) {
        logger.info("A method for visa processing techniques arrays");
        logger.debug("The number is transferred to the method from the console = " + operation);
        switch (operation) {
            case 0:
                logger.info("Exit from the program");
                System.exit(0);
                break;
            case 1:
                logger.info("Printing of the third array, consisting of those elements " +
                        "that are present in both arrays");
                new MyController().createArrays();
                new OperationsOnArrays().presentInTwotArrays(arrFirst, arrSecond);
                break;
            case 2:
                logger.info("Printing of the third array, consisting of those elements " +
                        "that are present in only one array");
                new MyController().createArrays();
                new OperationsOnArrays().presentInOneArrays(arrFirst, arrSecond);
                break;
            case 3:
                logger.info("Printing in array all numbers that are repeated more than two times");
                new OperationsOnArrays().deleteDuplicatesOverTwoNumbers(new MyController().createArray());
                break;
            case 4:
                logger.info("Printing in array");
                new OperationsOnArrays().deleteDuplicateNumbers(new MyController().createArray());
                break;
            default:
        }
    }

    public static void myFactoryDroid(int operation) {
        logger.info("a method for visa processing methods for droids");
        logger.debug("The number is transferred to the method from the console = " + operation);
        switch (operation) {
            case 0:
                logger.info("Exit from the program");
                System.exit(0);
                break;
            case 1:
                logger.info("Create droids");
                new DroidFactory().createDroids();
                break;
            case 2:
                logger.info("Print droids");
                new DroidFactory().printDroids();
                break;
            case 3:
                logger.info("Delete droid");
                new DroidFactory().removeDroid();
                break;
        }
    }

    private void createArrays() {
        logger.info("Creating arrays");
        System.out.println("Enter the length of the first array");
        int lenghtFirst = scanner.nextInt();
        logger.info("Creating first array");
        arrFirst = new int[lenghtFirst];
        addItems(arrFirst);

        System.out.println("Enter the length of the second array");
        int lenghtSecond = scanner.nextInt();
        logger.info("Creating second array");
        arrSecond = new int[lenghtSecond];
        addItems(arrSecond);
    }

    private int[] createArray() {
        logger.info("Creating one array");
        System.out.println("Enter the length of the array");
        int lenghtArr = scanner.nextInt();
        int[] array = new int[lenghtArr];
        addItems(array);
        return array;
    }

    private void addItems(int[] arrFirst) {
        logger.info("Add items to array");
        for (int i = 0; i < arrFirst.length; i++) {
            System.out.println("Please enter number");
            arrFirst[i] = scanner.nextInt();
        }
    }
}
