package com.bubnii.view;

import com.bubnii.controller.MyController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

import static com.bubnii.model.log.ClassName.getCurrentClassName;

public class MainView {

    private Scanner scanner = new Scanner(System.in);
    private static final Logger logger = LogManager.getLogger(getCurrentClassName());

    public void selectionOfOperation() {
        logger.info("The method calls the array processing");
        while (true) {
            logger.info("Start of the cycle in the method selectionOfOperation()");
            System.out.println("Enter the number to select the operation!");
            System.out.println("Enter 1 to make an array of those elements that are present in both arrays.");
            System.out.println("Enter 2 consisting of those elements that are present in only one array.");
            System.out.println("Enter 3 to remove all duplicate numbers in the array more than twice.");
            System.out.println("Enter 4 to find in the array all series of identical elements that go " +
                    "in succession, and remove all but one element from them.");
            System.out.println("Enter 0 to end the program!");
            int operation = scanner.nextInt();
            logger.debug("number scanned from the console = " + operation);
            MyController.myFactory(operation);
        }
    }
    public void selectionOfOperationDroid(){
        logger.info("The method calls the queue processing");
        while (true) {
            logger.info("Start of the cycle in the method selectionOfOperationDroid()");
            System.out.println("Enter the number to select the operation!");
            System.out.println("Enter 1 to create droids.");
            System.out.println("Enter 2 to print droids.");
            System.out.println("Enter 3 to remove droids.");
            System.out.println("Enter 0 to end the program!");
            int operation = scanner.nextInt();
            logger.debug("number scanned from the console = " + operation);
            MyController.myFactoryDroid(operation);
        }
    }
}
