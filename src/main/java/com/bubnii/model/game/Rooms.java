package com.bubnii.model.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class Rooms {
    public Rooms() {

    }

    private List<Room> rooms;
    private Random random;

    public void setRooms() {
        rooms = new ArrayList();
        int res;
        for (int i = 0; i < 10; i++) {
            random = new Random();
            res = random.nextInt(100) + 1;
            if (res >= 50) {
                rooms.add(new Room(new Artifact()));
            } else {
                rooms.add(new Room(new Monster()));
            }
        }
    }

    public List<Room> getInsiders() {
        return rooms;
    }

    public void showRooms() {
        int counter = 1;
        for (Room room : rooms) {
            System.out.println(
                    "Room #" + counter + " inside: " + room.getRoomInsiderName() + " with power: " + room
                            .getRoomInsider().getPower());
            counter++;
        }
    }
    public List<Room> getRooms(){
        return rooms;
    }
}