package com.bubnii.model.game;

import java.util.Random;

public class Monster extends RoomInsider{
    private Random random;
    private int power;
    Monster(){
        random = new Random();
        this.power = 5 + random.nextInt(95);
    }

    public int getPower() {
        return power;
    }
}