package com.bubnii.model.game;

public class Hero {
    private int power = 25;
    Hero(){}

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void addPower(int upgradePower){
        this.power += upgradePower;
    }
}