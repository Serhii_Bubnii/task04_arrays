package com.bubnii.model.game;

import java.util.Random;

public class Artifact extends RoomInsider{
    private Random random;
    private int power;
    Artifact(){
        random = new Random();
        this.power = 10 + random.nextInt(70);
    }

    public int getPower() {
        return power;
    }
}