package com.bubnii.model.game;

public class Output {
    public static void main(String[] args) {
        Hero hero = new Hero();
        Rooms rooms = new Rooms();
        rooms.setRooms(); // 10 rooms with random insiders
        rooms.showRooms();
        System.out.println("Number of possible death-rooms: " + GameCalculator.calculateDeath(hero, rooms));
        System.out.println("To be alive as much as possible you need: ");
        GameCalculator.showWinStrategy(hero, rooms);
    }
}
