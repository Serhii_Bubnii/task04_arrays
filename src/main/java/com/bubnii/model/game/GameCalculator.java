package com.bubnii.model.game;

import java.util.ArrayList;
import java.util.List;

public class GameCalculator {
    private GameCalculator() {
    }

    public static int calculateDeath(Hero hero, Rooms rooms) {
        int count = 0;
        for (Room r:rooms.getRooms()) {
            if (r.getRoomInsider() instanceof Monster) {
                if (hero.getPower() < r.getRoomInsider().getPower()) {
                    count++;
                }
            }
        }
        return count;
    }
    public static void showWinStrategy(Hero hero, Rooms rooms){
        List<Integer> list = new ArrayList();
        int counter = 1;
        int power = 25;
        for (Room r:rooms.getRooms()) {
            if(r.getRoomInsider() instanceof Artifact){
                list.add(counter);
                power += r.getRoomInsider().getPower();
            }
            counter++;
        }
        counter = 1;
        for (Room r:rooms.getRooms()) {
            if(r.getRoomInsider() instanceof Monster){
                if(r.getRoomInsider().getPower() <= power){
                    list.add(counter);
                }
            }
            counter++;
        }
        for (Integer i:list) {
            System.out.println("Go to room #"+i);
        }
    }
}
