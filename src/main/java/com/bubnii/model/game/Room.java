package com.bubnii.model.game;

public class Room {
    RoomInsider insider;
    Room(RoomInsider roomInsider){
        this.insider = roomInsider;
    }
    public RoomInsider getRoomInsider(){
        return this.insider;
    }
    public String getRoomInsiderName(){
        return this.insider.getClass().getSimpleName();
    }
}
