package com.bubnii.model.game;

public abstract class RoomInsider {
    private int power;

    public abstract int getPower();
}
