package com.bubnii.model.queue;

public interface ShipPriorityQueue<E> {
    int LENGTH_QUEUE_SHIP = 5;

    int addDroidToEndQueue(E e);

    int removeDroidFromBeginQueue();

    E printQueueDroid();
}
