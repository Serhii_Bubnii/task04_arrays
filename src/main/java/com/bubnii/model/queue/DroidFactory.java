package com.bubnii.model.queue;

import com.bubnii.model.generic.AbstractDroid;
import com.bubnii.model.generic.AlphaDroid;
import com.bubnii.model.generic.BetaDroid;
import com.bubnii.model.generic.GammaDroid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.bubnii.model.log.ClassName.getCurrentClassName;

public class DroidFactory {

    private static MyPriorityQueue<AbstractDroid> myPriorityQueue = new MyPriorityQueue<>();
    private static final Logger logger = LogManager.getLogger(getCurrentClassName());

    public void createDroids() {
        logger.info("create five droids");
        logger.info("create first droid");
        AbstractDroid droid1 = new AlphaDroid("Tod", 1);
        logger.info("create second droid");
        AbstractDroid droid2 = new BetaDroid("Bob", 2);
        logger.info("create third droid");
        AbstractDroid droid3 = new GammaDroid("Mark", 3);
        logger.info("create fourth droid");
        AbstractDroid droid4 = new AlphaDroid("Ell", 4);
        logger.info("create fifth droid");
        AbstractDroid droid5 = new AlphaDroid("Ell", 5);
        logger.info("adding droids to the queue");
        logger.info("adding first droid");
        myPriorityQueue.addDroidToEndQueue(droid1);
        logger.info("adding second droid");
        myPriorityQueue.addDroidToEndQueue(droid2);
        logger.info("adding third droid");
        myPriorityQueue.addDroidToEndQueue(droid3);
        logger.info("adding fourth droid");
        myPriorityQueue.addDroidToEndQueue(droid4);
        logger.info("adding fifth droid");
        myPriorityQueue.addDroidToEndQueue(droid5);
    }

    public void printDroids() {
        logger.info("printing all droids");
        System.out.println(myPriorityQueue.printQueueDroid());
    }

    public void removeDroid(){
        logger.info("remove one droid");
        myPriorityQueue.removeDroidFromBeginQueue();
    }
}
