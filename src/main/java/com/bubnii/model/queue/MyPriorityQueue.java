package com.bubnii.model.queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.bubnii.model.log.ClassName.getCurrentClassName;

public class MyPriorityQueue<E> implements ShipPriorityQueue<E> {

    private static final Logger logger = LogManager.getLogger(getCurrentClassName());
    private static final int NO_SHIP_IN_ARRAY = -1;
    private int indexShipInDroid = NO_SHIP_IN_ARRAY;
    private Object[] arrayShip = new Object[LENGTH_QUEUE_SHIP];


    @Override
    public int addDroidToEndQueue(E e) {
        logger.info("add droid to end queue, droid = " + e);
        if (indexShipInDroid == LENGTH_QUEUE_SHIP - 1) {
            return -1;
        }

        indexShipInDroid += 1;
        arrayShip[indexShipInDroid] = e;
        logger.info("return indexShipInDroid" + indexShipInDroid);
        return indexShipInDroid;
    }

    @Override
    public int removeDroidFromBeginQueue() {
        logger.info("remove droid from begin queue");
        if (indexShipInDroid < 0) {
            return -1;
        }

        for (int i = 0; i < indexShipInDroid; i++) {
            arrayShip[i] = arrayShip[i + 1];
        }
        logger.info("indexShipInDroid = " + indexShipInDroid);
        arrayShip[indexShipInDroid] = null;
        indexShipInDroid -= 1;
        return 1;
    }

    @Override
    public E printQueueDroid() {
        logger.info("print queue droid");
        String result = "";
        if (indexShipInDroid < 0) {
            logger.info("queue empty");
            return (E) "QueueEmpty";
        }
        for (int i = 0; i <= indexShipInDroid; i++) {
            result += "{" + arrayShip[i].toString() + "}\n";
        }
        return (E) result;
    }
}