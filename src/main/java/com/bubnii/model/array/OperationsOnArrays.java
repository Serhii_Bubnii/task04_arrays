package com.bubnii.model.array;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import static com.bubnii.model.log.ClassName.getCurrentClassName;
import static com.google.common.primitives.Ints.toArray;
import static java.util.Arrays.sort;

public class OperationsOnArrays {

    private static final Logger logger = LogManager.getLogger(getCurrentClassName());

    public int[] presentInTwotArrays(int[] firstArray, int[] secondArray) {
        logger.info("Formation of a third array consisting of those elements" +
                " that are present in both arrays");
        List<Integer> result = new ArrayList<>();
        for (int valueOne : firstArray) {
            for (int i : secondArray) {
                if (valueOne == i) {
                    result.add(valueOne);
                }
            }
        }
        System.out.println("Array: " + result);
        return toArray(result);
    }

    public int[] presentInOneArrays(int[] firstArray, int[] secondArray) {
        logger.info("Formation of a third array, consisting of those elements " +
                "that are present in only one array");
        List<Integer> result = new ArrayList<>();
        sort(firstArray);
        sort(secondArray);
        int i = 0;
        int j = 0;
        logger.info("we enter the cycle");
        while (i < firstArray.length && j < secondArray.length) {
            logger.info("If not common, print smaller");
            if (firstArray[i] < secondArray[j]) {
                result.add(firstArray[i]);
                i++;
            } else if (secondArray[j] < firstArray[i]) {
                result.add(secondArray[j]);
                j++;
            } else {
                logger.info("Skip common element");
                i++;
                j++;
            }
        }
        logger.info("printing remaining elements from the first array");
        while (i < firstArray.length) {
            result.add(firstArray[i]);
            i++;
        }
        logger.info("printing remaining elements from the second array");
        while (j < secondArray.length) {
            result.add(secondArray[j]);
            j++;
        }
        System.out.println("Array: " + result);
        return toArray(result);
    }

    public int[] deleteDuplicatesOverTwoNumbers(int[] array) {
        logger.info("Delete in array all numbers that are repeated " +
                "more than two times.");
        List<Integer> list = new ArrayList<>();
        int count;
        logger.info("we enter the first cycle");
        for (int i = 0; i < array.length - 1; i++) {
            count = 0;
            logger.info("we enter the second cycle");
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    count++;
                    if (count >= 2) {
                        list.add(array[i]);
                    }
                }
            }
        }
        System.out.println("Array: " + list);
        return toArray(list);
    }


    public int[] deleteDuplicateNumbers(int[] array) {
        logger.info("Finds in the array all series of identical elements that " +
                "follow in a row, and remove all of them from one element except one.");
        List<Integer> list = new ArrayList<>();
        logger.info("we enter the cycle");
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] != array[i + 1]) {
                list.add(array[i]);
            }
        }
        list.add(array[array.length - 1]);
        System.out.println("Array: " + list);
        return toArray(list);
    }
}
