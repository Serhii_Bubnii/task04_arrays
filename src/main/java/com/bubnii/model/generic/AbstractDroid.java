package com.bubnii.model.generic;

public abstract class AbstractDroid {
    protected String name;
    protected int level;

    public AbstractDroid(String name, int level) {
        this.name = name;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "name='" + name + '\'' +
                ", level=" + level +
                '}';
    }
}
